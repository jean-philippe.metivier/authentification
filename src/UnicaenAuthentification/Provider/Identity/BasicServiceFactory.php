<?php

namespace UnicaenAuthentification\Provider\Identity;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Basic identity provider factory.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class BasicServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $user              = $container->get('zfcuser_user_service');
        $config            = $container->get('BjyAuthorize\Config');
        $identityProvider  = new Basic($user->getAuthService());

        $identityProvider->setDefaultRole($config['default_role']);
        $identityProvider->setAuthenticatedRole($config['authenticated_role']);

        return $identityProvider;
    }
}