<?php

namespace UnicaenAuthentification\Controller;

use Exception;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use UnicaenAuthentification\Entity\Shibboleth\ShibUser;
use UnicaenUtilisateur\Formatter\RoleFormatter;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\Traits\ShibServiceAwareTrait;
use UnicaenAuthentification\Service\Traits\UserContextServiceAwareTrait;
use UnicaenAuthentification\Service\UserContext;
use Zend\Authentication\AuthenticationService;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use ZfcUser\Mapper\UserInterface;

/**
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 *
 * @method FlashMessenger flashMessenger()
 */
class UtilisateurController extends AbstractActionController
{
    use UserContextServiceAwareTrait;
    use ShibServiceAwareTrait;

    /**
     * @var LdapPeopleMapper
     */
    protected $ldapPeopleMapper;

    /**
     * @param LdapPeopleMapper $ldapPeopleMapper
     */
    public function setLdapPeopleMapper(LdapPeopleMapper $ldapPeopleMapper)
    {
        $this->ldapPeopleMapper = $ldapPeopleMapper;
    }

    /**
     * @var UserInterface
     */
    private $userMapper;

    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * @param UserInterface $userMapper
     */
    public function setUserMapper(UserInterface $userMapper)
    {
        $this->userMapper = $userMapper;
    }

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param ModuleOptions $options
     */
    public function setOptions(ModuleOptions $options)
    {
        $this->options = $options;
    }

    /**
     * Traite les requêtes AJAX POST de sélection d'un profil utilisateur.
     * La sélection est mémorisé en session par le service AuthUserContext.
     *
     * @param bool $addFlashMessage
     * @return bool|string
     */
    public function selectionnerProfilAction($addFlashMessage = true)
    {
        $request = $this->getRequest();
        if (! $request instanceof Request) {
            exit(1);
        }
        if (! $request->isXmlHttpRequest()) {
            return $this->redirect()->toRoute('home');
        }
        
        $role = $request->getPost('role');
        
        if ($role) {
            $this->serviceUserContext->setSelectedIdentityRole($role);
        }

        if ($addFlashMessage) {
            $selectedRole = $this->serviceUserContext->getSelectedIdentityRole();
            $message = sprintf(
                "Vous endossez à présent le rôle utilisateur '<strong>%s</strong>'.",
                (new RoleFormatter())->format($selectedRole)
            );
            $this->flashMessenger()->setNamespace('UnicaenAuth/success')->addMessage($message);
        }

        return false;
    }

    /**
     * Usurpe l'identité d'un autre utilisateur.
     *
     * @return Response
     */
    public function usurperIdentiteAction(): Response
    {
        $request = $this->getRequest();
        if (! $request instanceof Request) {
            exit(1);
        }

        $usernameUsurpe = $request->getQuery('identity', $request->getPost('identity'));
        if (! $usernameUsurpe) {
            return $this->redirect()->toRoute('home');
        }

        $utilisateurUsurpe = $this->getUserMapper()->findByUsername($usernameUsurpe); /** @var AbstractUser $utilisateurUsurpe */
        if ($utilisateurUsurpe === null) {
            $this->flashMessenger()->addErrorMessage(
                "La demande d'usurpation du compte '$usernameUsurpe' a échoué car aucun compte utilisateur correspondant " .
                "n'a été trouvé."
            );
            return $this->redirect()->toRoute('home');
        }

        $sessionIdentity = $this->serviceUserContext->usurperIdentite($usernameUsurpe);
        if ($sessionIdentity !== null) {
            // cuisine spéciale si l'utilisateur courant s'est authentifié via Shibboleth
            $this->usurperIdentiteShib($utilisateurUsurpe);
        }

        return $this->redirect()->toRoute('home');
    }

    /**
     * Cuisine spéciale pour l'usurpation si l'utilisateur courant s'est authentifié via Shibboleth.
     *
     * @param AbstractUser $utilisateurUsurpe Utilisateur à usurper
     */
    protected function usurperIdentiteShib(AbstractUser $utilisateurUsurpe)
    {
        $currentIdentityArray = $this->serviceUserContext->getIdentity();

        if (isset($currentIdentityArray['shib']) && $currentIdentityArray['shib'] instanceof ShibUser) {
            $fromShibUser = $currentIdentityArray['shib'];
            $this->shibService->activateUsurpation($fromShibUser, $utilisateurUsurpe);
        }
    }

    /**
     * Met fin à l'usurpation d'identité en cours éventuelle.
     *
     * @return Response
     */
    public function stopperUsurpationAction(): Response
    {
        $this->serviceUserContext->stopperUsurpation();

        // cuisine spéciale si l'utilisateur courant s'est authentifié via Shibboleth
        $this->stopperUsurpationIdentiteShib();

        return $this->redirect()->toRoute('home');
    }

    /**
     * Cuisine spéciale pour stopper l'usurpation si l'utilisateur courant s'est authentifié via Shibboleth.
     */
    protected function stopperUsurpationIdentiteShib()
    {
        $currentIdentityArray = $this->serviceUserContext->getIdentity();

        if (isset($currentIdentityArray['shib']) && $currentIdentityArray['shib'] instanceof ShibUser) {
            $this->shibService->deactivateUsurpation();
        }
    }

    /**
     * @return UserInterface
     */
    public function getUserMapper(): UserInterface
    {
        return $this->userMapper;
    }

    /**
     * @return UserContext
     * @deprecated Utiliser $this->serviceUserContext directement, svp.
     */
    protected function getAuthUserContextService(): UserContext
    {
        return $this->serviceUserContext;
    }

    /**
     * @return LdapPeopleMapper
     */
    public function getLdapPeopleMapper(): LdapPeopleMapper
    {
        return $this->ldapPeopleMapper;
    }
}