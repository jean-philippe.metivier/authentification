<?php

namespace UnicaenAuthentification\View\Helper;

use Zend\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le formulaire d'authentification locale (ldap ou db),
 * si l'authentification locale est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class LocalConnectViewHelper extends AbstractConnectViewHelper
{
    const TYPE = 'local';
    const TITLE = "Avec un compte local (ldap/db)";

    public function __construct()
    {
        $this->setType(self::TYPE);
    }
}