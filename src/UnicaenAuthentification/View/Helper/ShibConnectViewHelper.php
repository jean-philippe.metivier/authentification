<?php

namespace UnicaenAuthentification\View\Helper;

use Zend\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le bouton de connexion via Shibboleth,
 * si l'authentification Shibboleth est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class ShibConnectViewHelper extends AbstractConnectViewHelper
{
    const TYPE = 'shib';
    const TITLE = "Via la fédération d'identité";

    public function __construct()
    {
        $this->setType(self::TYPE);
    }
}