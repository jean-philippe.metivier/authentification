<?php

namespace UnicaenAuthentification\View\Helper;

/**
 * Aide de vue dessinant le formulaire d'authentification LDAP,
 * si l'authentification LDAP est activée.
 *
 * @author Unicaen
 * @deprecated Use {@see LocalConnectViewHelper}
 */
class LdapConnectViewHelper extends AbstractConnectViewHelper
{
    public function __construct()
    {
        $this->setType('ldap');
        $this->setTitle("Avec mon compte établissement");
    }
}