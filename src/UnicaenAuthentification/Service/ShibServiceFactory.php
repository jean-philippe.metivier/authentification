<?php

namespace UnicaenAuthentification\Service;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;

class ShibServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        $service = new ShibService();
        $service->setShibbolethConfig($moduleOptions->getShib());
        $service->setUsurpationAllowedUsernames($moduleOptions->getUsurpationAllowedUsernames());

        return $service;
    }
}