<?php

namespace UnicaenAuthentification\Service;

use Exception;
use phpCAS;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenApp\Entity\Ldap\People as LdapPeople;
use UnicaenAuthentification\Options\ModuleOptions;
use Zend\Router\RouteInterface;
use Zend\Router\RouteStackInterface;
use ZfcUser\Authentication\Adapter\ChainableAdapter;

class CasService
{
    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * @var array
     */
    protected $casOptions;

    /**
     * @var phpCAS
     */
    protected $casClient;

    /**
     * @var LdapPeopleMapper
     */
    protected $ldapPeopleMapper;

    /**
     * @var User
     */
    private $userService;

    /**
     * @param User $userService
     */
    public function setUserService(User $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @var RouteInterface
     */
    private $router;

    /**
     * @param RouteInterface $router
     */
    public function setRouter(RouteInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Réalise l'authentification.
     *
     * @return LdapPeople|null
     */
    public function authenticate()
    {
        if (! $this->isCasEnabled()) {
            return null;
        }

        error_reporting($oldErrorReporting = error_reporting() & ~E_NOTICE);

        $this->getCasClient()->forceAuthentication();

        // at this step, the user has been authenticated by the CAS server
        // and the user's login name can be read with phpCAS::getUser().

        $identity = $this->getCasClient(false)->getUser();

        error_reporting($oldErrorReporting);

        // recherche de l'individu dans l'annuaire LDAP (il existe forcément puisque l'auth CAS a réussi)
        $ldapPeople = $this->getLdapPeopleMapper()->findOneByUsername($identity);

        /* @var $userService User */
        $this->userService->userAuthenticated($ldapPeople);

        return $ldapPeople;
    }

    /**
     * @return bool
     */
    public function isCasEnabled()
    {
        $config = $this->options->getCas();

        if (! $config) {
            return false;
        }
        if (isset($config['enabled'])) {
            return (bool) $config['enabled'];
        }

        return true;
    }

    /**
     *
     * @see ChainableAdapter
     */
    public function logout()
    {
        if (! $this->isCasEnabled()) {
            return;
        }

        $returnUrl = $this->router->getRequestUri()->setPath($this->router->getBaseUrl())->toString();
        $this->getCasClient()->logoutWithRedirectService($returnUrl);
    }

    /**
     * Retourne le client CAS.
     *
     * @param boolean $initClient
     * @return phpCAS
     * @throws Exception
     */
    public function getCasClient($initClient = true)
    {
        if (null === $this->casClient) {
            $this->casClient = new phpCAS();
        }

        if (!$initClient) {
            return $this->casClient;
        }

        if (null === $this->casOptions) {
            $config = $this->getOptions()->getCas();
            if (!isset($config['connection']['default']['params']) || !$config['connection']['default']['params']) {
                throw new Exception("Les paramètres de connexion au serveur CAS sont invalides.");
            }
            $this->casOptions = $config['connection']['default']['params'];
        }

        $options = $this->casOptions;

        if (array_key_exists('debug', $options) && (bool) $options['debug']) {
            $this->casClient->setDebug();
        }

        // initialize phpCAS
        $this->casClient->client($options['version'], $options['hostname'], $options['port'], $options['uri'], true);
        // no SSL validation for the CAS server
        $this->casClient->setNoCasServerValidation();

        return $this->casClient;
    }

    /**
     * Spécifie le client CAS.
     *
     * @param phpCAS $casClient
     * @return self
     */
    public function setCasClient(phpCAS $casClient)
    {
        $this->casClient = $casClient;
        return $this;
    }

    /**
     * @param ModuleOptions $options
     */
    public function setOptions(ModuleOptions $options)
    {
        $this->options = $options;
    }

    /**
     * @return ModuleOptions
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * get ldap people mapper
     *
     * @return LdapPeopleMapper
     */
    public function getLdapPeopleMapper()
    {
        return $this->ldapPeopleMapper;
    }

    /**
     * set ldap people mapper
     *
     * @param LdapPeopleMapper $mapper
     * @return self
     */
    public function setLdapPeopleMapper(LdapPeopleMapper $mapper)
    {
        $this->ldapPeopleMapper = $mapper;

        return $this;
    }

    /**
     * @param RouteInterface $router
     */
    public function reconfigureRoutesForCasAuth(RouteInterface $router)
    {
        if (! $this->isCasEnabled()) {
            return;
        }
        if (!$router instanceof RouteStackInterface) {
            return;
        }

        $router->addRoutes([
            // remplace les routes existantes (cf. config du module)
            'zfcuser' => [
                'type'          => 'Literal',
                'priority'      => 1000,
                'options'       => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => 'zfcuser',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'login'  => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/connexion[/:type]',
                            'defaults' => [
                                'controller' => 'zfcuser',
                                'action'     => 'authenticate', // zappe l'action 'login'
                            ],
                        ],
                    ],
                    'logout' => [
                        'type'    => 'Literal',
                        'options' => [
                            'route'    => '/deconnexion',
                            'defaults' => [
                                'controller' => 'zfcuser',
                                'action'     => 'logout',
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}