<?php

namespace UnicaenAuthentification\Service;

use Assert\Assertion;
use Assert\AssertionFailedException;
use InvalidArgumentException;
use UnicaenApp\Exception\LogicException;
use UnicaenApp\Exception\RuntimeException;
use UnicaenAuthentification\Entity\Shibboleth\ShibUser;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use Zend\Router\Http\TreeRouteStack;
use Zend\Session\Container;

/**
 * Shibboleth service.
 *
 * @author Unicaen
 */
class ShibService
{
    const SHIB_USER_ID_EXTRACTOR = 'shib_user_id_extractor';

    const DOMAIN_DEFAULT = 'default';

    const KEY_fromShibUser = 'fromShibUser';
    const KEY_toShibUser = 'toShibUser';

    /**
     * @var ShibUser
     */
    protected $authenticatedUser;

    /**
     * @var array
     */
    protected $shibbolethConfig = [];

    /**
     * @var array
     */
    protected $usurpationAllowedUsernames = [];

    /**
     * @return string
     */
    static public function apacheConfigSnippet(): string
    {
        $text = <<<EOS
<Location "/">
    AuthType Shibboleth
    ShibRequestSetting requireSession false
    Require shibboleth
</Location>
<Location "/auth/shibboleth">
        AuthType Shibboleth
        ShibRequestSetting requireSession true
        Require shibboleth
</Location>
EOS;
        return $text;
    }

    /**
     * @param array $shibbolethConfig
     */
    public function setShibbolethConfig(array $shibbolethConfig)
    {
        $this->shibbolethConfig = $shibbolethConfig;
    }

    /**
     * @param array $usurpationAllowedUsernames
     */
    public function setUsurpationAllowedUsernames(array $usurpationAllowedUsernames)
    {
        $this->usurpationAllowedUsernames = $usurpationAllowedUsernames;
    }

    /**
     * @return ShibUser|null
     */
    public function getAuthenticatedUser(): ?ShibUser
    {
        if (! $this->isShibbolethEnabled()) {
            return null;
        }

        if ($this->authenticatedUser === null) {

            // D'ABORD activation éventuelle de la simulation
            $this->handleSimulation();
            // ENSUITE activation éventuelle de l'usurpation
            $this->handleUsurpation();

            if (! $this->isAuthenticated()) {
                return null;
            }

            $this->authenticatedUser = $this->createShibUserFromServerArrayData();
        }

        return $this->authenticatedUser;
    }

    /**
     * @return bool
     */
    private function isAuthenticated(): bool
    {
        return
            $this->getValueFromShibData('REMOTE_USER', $_SERVER) ||
            $this->getValueFromShibData('Shib-Session-ID', $_SERVER) ||
            $this->getValueFromShibData('HTTP_SHIB_SESSION_ID', $_SERVER);
    }

    /**
     * @return boolean
     */
    public function isShibbolethEnabled(): bool
    {
        return
            array_key_exists('enabled', $this->shibbolethConfig) && (bool) $this->shibbolethConfig['enabled'] ||
            array_key_exists('enable', $this->shibbolethConfig) && (bool) $this->shibbolethConfig['enable'];
    }

    /**
     * @return array
     */
    public function getShibbolethSimulate(): array
    {
        if (! array_key_exists('simulate', $this->shibbolethConfig) || ! is_array($this->shibbolethConfig['simulate'])) {
            return [];
        }

        return $this->shibbolethConfig['simulate'];
    }

    /**
     * @param string $attributeName
     * @return string
     */
    private function getShibbolethAliasFor(string $attributeName): ?string
    {
        if (! array_key_exists('aliases', $this->shibbolethConfig) ||
            ! is_array($this->shibbolethConfig['aliases']) ||
            ! isset($this->shibbolethConfig['aliases'][$attributeName])) {
            return null;
        }

        return $this->shibbolethConfig['aliases'][$attributeName];
    }

    /**
     * Retourne les alias des attributs spécifiés.
     * Si un attribut n'a pas d'alias, c'est l'attribut lui-même qui est retourné.
     *
     * @param array $attributeNames
     * @return array
     */
    private function getAliasedShibbolethAttributes(array $attributeNames): array
    {
        $aliasedAttributes = [];
        foreach ($attributeNames as $attributeName) {
            $alias = $this->getShibbolethAliasFor($attributeName);
            $aliasedAttributes[$attributeName] = $alias ?: $attributeName;
        }

        return $aliasedAttributes;
    }

    /**
     * Retourne true si la simulation d'un utilisateur authentifié via Shibboleth est en cours.
     *
     * @return bool
     */
    public function isSimulationActive(): bool
    {
        if (array_key_exists('simulate', $this->shibbolethConfig) &&
            is_array($this->shibbolethConfig['simulate']) &&
            ! empty($this->shibbolethConfig['simulate'])) {
            return true;
        }

        return false;
    }

    /**
     * Retourne la liste des attributs requis.
     *
     * @return array
     */
    private function getShibbolethRequiredAttributes(): array
    {
        if (! array_key_exists('required_attributes', $this->shibbolethConfig)) {
            return [];
        }

        return (array)$this->shibbolethConfig['required_attributes'];
    }

    /**
     * @return array
     */
    private function getShibUserIdExtractorDefaultConfig(): array
    {
        return $this->getShibUserIdExtractorConfigForDomain(self::DOMAIN_DEFAULT);
    }

    /**
     * Retourne la config permettant d'extraire l'id à partir des attributs.
     *
     * @param string $domain
     * @return array
     */
    private function getShibUserIdExtractorConfigForDomain(string $domain): array
    {
        $key = self::SHIB_USER_ID_EXTRACTOR;
        if (! array_key_exists($key, $this->shibbolethConfig)) {
            throw new RuntimeException("Aucune config '$key' trouvée.");
        }

        $config = $this->shibbolethConfig[$key];

        if (! array_key_exists($domain, $config)) {
            return [];
        }

        return $config[$domain];
    }

    /**
     *
     */
    public function handleSimulation()
    {
        if (! $this->isSimulationActive()) {
            return;
        }

        try {
            $shibUser = $this->createShibUserFromSimulationData();
        } catch (AssertionFailedException $e) {
            throw new LogicException("Configuration erronée", null, $e);
        }

        $this->simulateAuthenticatedUser($shibUser);
    }

    /**
     * @return ShibUser
     * @throws AssertionFailedException
     */
    private function createShibUserFromSimulationData(): ShibUser
    {
        $data = $this->getShibbolethSimulate();

        $this->assertRequiredAttributesExistInData($data);

        $eppn = $this->getValueFromShibData('eppn', $data);
        $email = $this->getValueFromShibData('mail', $data);
        $displayName = $this->getValueFromShibData('displayName', $data);
        $givenName = $this->getValueFromShibData('givenName', $data);
        $surname = $this->getValueFromShibData('sn', $data);
        $civilite = $this->getValueFromShibData('supannCivilite', $data);

        Assertion::contains($eppn, '@', "L'eppn '" . $eppn . "' n'est pas de la forme 'id@domaine' attendue (ex: 'tartempion@unicaen.fr')");

        $shibUser = new ShibUser();
        // propriétés de UserInterface
        $shibUser->setEppn($eppn);
        $shibUser->setUsername($eppn);
        $domain = $shibUser->getEppnDomain(); // possible uniquement après $shibUser->setEppn($eppn)
        $id = $this->extractShibUserIdValueForDomainFromShibData($domain, $data);
        $shibUser->setId($id);
        $shibUser->setDisplayName($displayName);
        $shibUser->setEmail($email);
        // autres propriétés
        $shibUser->setNom($surname);
        $shibUser->setPrenom($givenName);
        $shibUser->setCivilite($civilite);

        return $shibUser;
    }

    /**
     * Retourne true si les données stockées en session indiquent qu'une usurpation d'identité Shibboleth est en cours.
     *
     * @return bool
     */
    private function isUsurpationActive(): bool
    {
        return $this->getSessionContainer()->offsetExists(self::KEY_fromShibUser);
    }

    /**
     * @param ShibUser $fromShibUser
     * @param ShibUser $toShibUser
     * @return self
     */
    public function activateUsurpationOld(ShibUser $fromShibUser, ShibUser $toShibUser): self
    {
//        // le login doit faire partie des usurpateurs autorisés
//        if (! in_array($fromShibUser->getUsername(), $this->usurpationAllowedUsernames)) {
//            throw new RuntimeException("Usurpation non autorisée");
//        }

        $session = $this->getSessionContainer();
        $session->offsetSet(self::KEY_fromShibUser, $fromShibUser);
        $session->offsetSet(self::KEY_toShibUser, $toShibUser);

        return $this;
    }

    /**
     * @param ShibUser $currentShibUser
     * @param AbstractUser $utilisateurUsurpe
     * @return self
     */
    public function activateUsurpation(ShibUser $currentShibUser, AbstractUser $utilisateurUsurpe): self
    {
        if (! ShibUser::isEppn($utilisateurUsurpe->getUsername())) {
            // cas d'usurpation d'un compte local (db) depuis une authentification shib
            return $this;
        }

        $toShibUser = new ShibUser();
        $toShibUser->setEppn($utilisateurUsurpe->getUsername());
        $toShibUser->setId(uniqid()); // peut pas mieux faire pour l'instant
        $toShibUser->setDisplayName($utilisateurUsurpe->getDisplayName());
        $toShibUser->setEmail($utilisateurUsurpe->getEmail());
        $toShibUser->setNom('?');     // peut pas mieux faire pour l'instant
        $toShibUser->setPrenom('?');  // peut pas mieux faire pour l'instant

//        // le login doit faire partie des usurpateurs autorisés
//        if (! in_array($fromShibUser->getUsername(), $this->usurpationAllowedUsernames)) {
//            throw new RuntimeException("Usurpation non autorisée");
//        }

        $session = $this->getSessionContainer();
        $session->offsetSet(self::KEY_fromShibUser, $currentShibUser);
        $session->offsetSet(self::KEY_toShibUser, $toShibUser);

        return $this;
    }

    /**
     * Suppression des données stockées en session concernant l'usurpation d'identité Shibboleth.
     *
     * @return self
     */
    public function deactivateUsurpation(): self
    {
        $session = $this->getSessionContainer();
        $session->offsetUnset(self::KEY_fromShibUser);
        $session->offsetUnset(self::KEY_toShibUser);

        return $this;
    }

    /**
     * @return self
     */
    public function handleUsurpation(): self
    {
        if (! $this->isUsurpationActive()) {
            return $this;
        }

        $session = $this->getSessionContainer();

        /** @var ShibUser|null $toShibUser */
        $toShibUser = $session->offsetGet($key = self::KEY_toShibUser);
        if ($toShibUser === null) {
            throw new RuntimeException("Anomalie: clé '$key' introuvable");
        }

        $this->simulateAuthenticatedUser($toShibUser);

        return $this;
    }

    /**
     * @return Container
     */
    private function getSessionContainer(): Container
    {
        return new Container(ShibService::class);
    }

    /**
     * @param array $data
     * @return array
     */
    private function getMissingRequiredAttributesFromData(array $data): array
    {
        $requiredAttributes = $this->getShibbolethRequiredAttributes();
        $missingAttributes = [];

        foreach ($requiredAttributes as $requiredAttribute) {
            // un pipe permet d'exprimer un OU logique, ex: 'supannEmpId|supannEtuId'
            $attributes = array_map('trim', explode('|', $requiredAttribute));
            // attributs aliasés
            $attributes = $this->getAliasedShibbolethAttributes($attributes);

            $found = false;
            foreach (array_map('trim', $attributes) as $attribute) {
                if (isset($data[$attribute])) {
                    $found = true;
                }
            }
            if (!$found) {
                // attributs aliasés, dont l'un au moins est manquant, mise sous forme 'a|b'
                $missingAttributes[] = implode('|', $attributes);
            }
        }

        return $missingAttributes;
    }

    /**
     * @param array $data
     * @throws InvalidArgumentException
     */
    private function assertRequiredAttributesExistInData(array $data)
    {
        $missingAttributes = $this->getMissingRequiredAttributesFromData($data);

        if (!empty($missingAttributes)) {
            throw new InvalidArgumentException(
                "Les attributs suivants sont manquants : " . implode(', ', $missingAttributes));
        }
    }

    /**
     * Inscrit dans le tableau $_SERVER le nécessaire pour usurper l'identité d'un utilisateur
     * qui se serait authentifié via Shibboleth.
     *
     * @param ShibUser $shibUser Utilisateur dont on veut usurper l'identité.
     */
    public function simulateAuthenticatedUser(ShibUser $shibUser)
    {
        // 'REMOTE_USER' (notamment) est utilisé pour savoir si un utilisateur est authentifié ou non
        $this->setServerArrayVariable('REMOTE_USER', $shibUser->getEppn());

        // pour certains attributs, on veut une valeur sensée!
        $this->setServerArrayVariable('eppn', $shibUser->getEppn());
        $this->setServerArrayVariable('supannEmpId', $shibUser->getId()); // ou bien 'supannEtuId', peu importe.
        $this->setServerArrayVariable('displayName', $shibUser->getDisplayName());
        $this->setServerArrayVariable('mail', $shibUser->getEppn());
        $this->setServerArrayVariable('sn', $shibUser->getNom());
        $this->setServerArrayVariable('givenName', $shibUser->getPrenom());
        $this->setServerArrayVariable('supannCivilite', $shibUser->getCivilite());
    }

    /**
     * @return ShibUser
     */
    private function createShibUserFromServerArrayData(): ShibUser
    {
        try {
            $this->assertRequiredAttributesExistInData($_SERVER);
        } catch (InvalidArgumentException $e) {
            throw new RuntimeException('Des attributs Shibboleth obligatoires font défaut dans $_SERVER.', null, $e);
        }

        $eppn = $this->getValueFromShibData('eppn', $_SERVER);
        $mail = $this->getValueFromShibData('mail', $_SERVER);
        $displayName = $this->getValueFromShibData('displayName', $_SERVER);
        $surname = $this->getValueFromShibData('sn', $_SERVER) ?: $this->getValueFromShibData('surname', $_SERVER);
        $givenName = $this->getValueFromShibData('givenName', $_SERVER);
        $civilite = $this->getValueFromShibData('supannCivilite', $_SERVER);

        $shibUser = new ShibUser();
        // propriétés de UserInterface
        $shibUser->setEppn($eppn);
        $shibUser->setUsername($eppn);
        $domain = $shibUser->getEppnDomain(); // possible uniquement après $shibUser->setEppn($eppn)
        $id = $this->extractShibUserIdValueForDomainFromShibData($domain, $_SERVER);
        $shibUser->setId($id);
        $shibUser->setDisplayName($displayName);
        $shibUser->setEmail($mail);
        $shibUser->setPassword(null);
        // autres propriétés
        $shibUser->setNom($surname);
        $shibUser->setPrenom($givenName);
        $shibUser->setCivilite($civilite);

        return $shibUser;
    }

    /**
     * @param string $domain
     * @param array $data
     * @return string|null
     */
    private function extractShibUserIdValueForDomainFromShibData(string $domain, array $data): ?string
    {
        $config = $this->getShibUserIdExtractorConfigForDomain($domain);
        if (empty($config)) {
            $config = $this->getShibUserIdExtractorDefaultConfig();
            if (empty($config)) {
                throw new RuntimeException("Aucune config trouvée ni pour le domaine '$domain' ni par défaut.");
            }
        }

        foreach ($config as $array) {
            $name = $array['name'];
            $value = $this->getValueFromShibData($name, $data) ?: null;
            if ($value !== null) {
                $pregMatchPattern = $array['preg_match_pattern'] ?? null;
                if ($pregMatchPattern !== null) {
                    if (preg_match($pregMatchPattern, $value, $matches) === 0) {
                        throw new RuntimeException("Le pattern '$pregMatchPattern' n'a pas permis d'extraire une valeur de '$name'.");
                    }
                    $value = $matches[1];
                }

                return $value;
            }
        }

        return null;
    }

    /**
     * Retourne l'URL de déconnexion Shibboleth.
     *
     * @param string $returnAbsoluteUrl Eventuelle URL *absolue* de retour après déconnexion
     * @return string
     */
    public function getLogoutUrl($returnAbsoluteUrl = null): string
    {
        if ($this->getShibbolethSimulate()) {
            return '/';
        }
        if ($this->getAuthenticatedUser() === null) {
            return '/';
        }

        $logoutUrl = $this->shibbolethConfig['logout_url'];

        if ($returnAbsoluteUrl) {
            $logoutUrl .= urlencode($returnAbsoluteUrl);
        }

        return $logoutUrl;
    }

    /**
     * @param TreeRouteStack $router
     */
    public function reconfigureRoutesForShibAuth(TreeRouteStack $router)
    {
        if (! $this->isShibbolethEnabled()) {
            return;
        }

        $router->addRoutes([
            // remplace les routes existantes (cf. config du module)
            'zfcuser' => [
                'type'          => 'Literal',
                'priority'      => 1000,
                'options'       => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => 'zfcuser',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'login' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/connexion[/:type]',
                            'defaults' => [
                                'controller' => 'zfcuser', // NB: lorsque l'auth Shibboleth est activée, la page propose
                                'action'     => 'login',   //     2 possibilités d'auth : LDAP et Shibboleth.
                            ],
                        ],
                    ],
                    'logout' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/:operation/shibboleth/',
                            'defaults' => [
                                'controller' => 'UnicaenAuthentification\Controller\Auth',
                                'action'     => 'shibboleth',
                                'operation'  => 'deconnexion'
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param string $name
     * @param array $data
     * @return string
     */
    private function getValueFromShibData(string $name, array $data): ?string
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        if (! array_key_exists($key, $data)) {
            return null;
        }

        return $data[$key];
    }

    /**
     * @param string $name
     * @param string|null $value
     */
    private function setServerArrayVariable(string $name, string $value = null)
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        $_SERVER[$key] = $value;
    }
}