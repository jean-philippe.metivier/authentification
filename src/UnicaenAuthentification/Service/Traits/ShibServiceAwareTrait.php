<?php

namespace UnicaenAuthentification\Service\Traits;

use UnicaenAuthentification\Service\ShibService;

trait ShibServiceAwareTrait
{
    /**
     * @var ShibService
     */
    protected $shibService;

    /**
     * @param ShibService $shibService
     */
    public function setShibService(ShibService $shibService)
    {
        $this->shibService = $shibService;
    }
}