<?php

namespace UnicaenAuthentification\Service\Traits;

use UnicaenAuthentification\Service\User as UserService;

trait UserServiceAwareTrait
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     */
    public function setUserService(UserService $userService)
    {
        $this->userService = $userService;
    }
}