<?php

namespace UnicaenAuthentification\Service;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Entity\Ldap\People;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Entity\UserInterface;

/**
 * Initialisateur chargé d'injecter l'utilisateur courant dans les services en ayant besoin.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class UserAwareInitializer implements InitializerInterface
{
    /**
     * Test d'éligibilité.
     * 
     * @param mixed $instance
     * @return bool
     */
    protected function canInitialize($instance)
    {
        return $instance instanceof DbUserAwareInterface || $instance instanceof LdapUserAwareInterface;
    }

    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        $this->__invoke($serviceLocator, $instance);
    }

    public function __invoke(ContainerInterface $container, $instance)
    {
        // test d'éligibilité à faire au plus tôt pour éviter l'erreur
        // 'Circular dependency for LazyServiceLoader was found for instance Zend\Authentication\AuthenticationService'
        if (!$this->canInitialize($instance)) {
            return;
        }

        $authenticationService = $container->get(AuthenticationService::class);
        if (!$authenticationService->hasIdentity()) {
            return;
        }

        $identity = $authenticationService->getIdentity();

        if ($instance instanceof DbUserAwareInterface) {
            if (isset($identity['db']) && $identity['db'] instanceof UserInterface) {
                $instance->setDbUser($identity['db']);
            }
        }

        if ($instance instanceof LdapUserAwareInterface) {
            if (isset($identity['ldap']) && $identity['ldap'] instanceof People) {
                $instance->setLdapUser($identity['ldap']);
            }
        }
    }
}