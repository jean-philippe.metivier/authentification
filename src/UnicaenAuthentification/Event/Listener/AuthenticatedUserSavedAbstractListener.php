<?php

namespace UnicaenAuthentification\Event\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use UnicaenApp\Service\EntityManagerAwareInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenAuthentification\Event\UserAuthenticatedEvent;

/**
 * Classe abstraites pour les classes désirant scruter un événement déclenché lors de l'authentification
 * utilisateur.
 *
 * Événements disponibles :
 * - juste avant que l'entité utilisateur ne soit persistée.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see UserAuthenticatedEvent
 */
abstract class AuthenticatedUserSavedAbstractListener implements ListenerAggregateInterface, EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = [];

    /**
     * Méthode appelée juste avant que l'entité utilisateur soit persistée.
     *
     * @param UserAuthenticatedEvent $e
     */
    public function onUserAuthenticatedPrePersist(UserAuthenticatedEvent $e)
    {

    }

    /**
     * Méthode appelée juste après que l'entité utilisateur soit persistée.
     *
     * @param UserAuthenticatedEvent $e
     */
    public function onUserAuthenticatedPostPersist(UserAuthenticatedEvent $e)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEvents = $events->getSharedManager();

        $this->listeners[] = $sharedEvents->attach(
            'UnicaenAuthentification\Service\User',
            UserAuthenticatedEvent::PRE_PERSIST,
            [$this, 'onUserAuthenticatedPrePersist'],
            100);

        $this->listeners[] = $sharedEvents->attach(
            'UnicaenAuthentification\Service\User',
            UserAuthenticatedEvent::POST_PERSIST,
            [$this, 'onUserAuthenticatedPostPersist'],
            100);
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
}