<?php

namespace UnicaenAuthentification\Options;

/**
 * Classe encapsulant les options de fonctionnement du module.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class ModuleOptions extends \ZfcUser\Options\ModuleOptions
{
    /**
     * Paramètres concernant l'authentification locale propre à l'appli.
     *
     * @var array
     * @deprecated Use local['db']
     */
    protected $db = [];

    /**
     * Paramètres concernant l'authentification LDAP.
     *
     * @var array
     * @deprecated Use local['ldap']
     */
    protected $ldap = [];

    /**
     * @var array
     */
    protected $usurpationAllowedUsernames = [];

    /**
     * @var bool
     */
    protected $saveLdapUserInDatabase = false;

    /**
     * @var string
     */
    protected $ldapUsername;

    /**
     * @var array
     */
    protected $local = [];

    /**
     * @var array
     */
    protected $shib = [];

    /**
     * @var array
     */
    protected $cas = [];

    /**
     * @var string
     */
    protected $entityManagerName = 'doctrine.entitymanager.orm_default';

    /**
     * @var array
     */
    protected $authTypes;

    /**
     * @var array|\Traversable|null
     */
    protected $originalOptions;

    /**
     * @inheritDoc
     */
    public function __construct($options = null)
    {
        parent::__construct($options);

        /** On conserve le tableau de config original, @see __get(). */
        $this->originalOptions = $options;
    }

    /**
     * @param array $authTypes
     * @return self
     */
    public function setAuthTypes(array $authTypes): self
    {
        $this->authTypes = $authTypes;
        return $this;
    }

    /**
     * @return array
     */
    public function getLocal(): array
    {
        return $this->local;
    }

    /**
     * @param array $local
     * @return self
     */
    public function setLocal(array $local): self
    {
        $this->local = $local;
        return $this;
    }

    /**
     * @return array
     */
    public function getDb(): array
    {
        return $this->local['db'];
    }

    /**
     * @param array $config
     * @return self
     */
    public function setDb(array $config): self
    {
        $this->db = $config;
        $this->local['db'] = $config;

        return $this;
    }

    /**
     * Retourne les paramètres concernant l'authentification LDAP.
     *
     * @return array
     */
    public function getLdap(): array
    {
        return $this->local['ldap'];
    }

    /**
     * Spécifie les paramètres concernant l'authentification LDAP.
     *
     * @param array $ldap
     * @return self
     */
    public function setLdap(array $ldap): self
    {
        $this->ldap = $ldap;
        $this->local['ldap'] = $ldap;

        return $this;
    }

    /**
     * Configs ordonnées des types d'authentification activés.
     *
     * @return array[] Exemple : ['local' => ['enabled'=>true, ...], 'shib' => ['enabled'=>true, ...]]
     */
    public function getEnabledAuthTypes(): array
    {
        $array = [];
        foreach ($this->authTypes as $authType) {
            /** @var array $authTypeConfig */
            $authTypeConfig = $this->__get($authType);
            $array[$authType] = $authTypeConfig;
        }

        $array = array_filter($array, function(array $config) {
            return
                isset($config['enabled']) and (bool) $config['enabled'] or
                isset($config['enable']) and (bool) $config['enable'];
        });
        uasort($array, function($a, $b) {
            return ($a['order'] ?? 0) - ($b['order'] ?? 0);
        });

        return $array;
    }

    /**
     * set usernames allowed to make usurpation
     *
     * @param array $usurpationAllowedUsernames
     * @return self
     */
    public function setUsurpationAllowedUsernames(array $usurpationAllowedUsernames = []): self
    {
        $this->usurpationAllowedUsernames = $usurpationAllowedUsernames;

        return $this;
    }

    /**
     * get usernames allowed to make usurpation
     *
     * @return array
     */
    public function getUsurpationAllowedUsernames(): array
    {
        return $this->usurpationAllowedUsernames;
    }

    /**
     * Spécifie si l'utilisateur authentifié doit être enregistré dans la base
     * de données de l'appli
     *
     * @param bool $flag
     * @return self
     */
    public function setSaveLdapUserInDatabase($flag = true): self
    {
        $this->saveLdapUserInDatabase = (bool)$flag;

        return $this;
    }

    /**
     * Retourne la valeur du flag spécifiant si l'utilisateur authentifié doit être
     * enregistré dans la base de données de l'appli
     *
     * @return bool
     */
    public function getSaveLdapUserInDatabase(): bool
    {
        return $this->saveLdapUserInDatabase;
    }

    /**
     * @return string
     */
    public function getLdapUsername(): string
    {
        return $this->ldapUsername;
    }

    /**
     * @param string $ldapUsername
     * @return self
     */
    public function setLdapUsername(string $ldapUsername): self
    {
        $this->ldapUsername = $ldapUsername;

        return $this;
    }

    /**
     * set cas connection params
     *
     * @param array $cas
     * @return self
     */
    public function setCas(array $cas = []): self
    {
        $this->cas = $cas;

        return $this;
    }

    /**
     * get cas connection params
     *
     * @return array
     */
    public function getCas(): array
    {
        return $this->cas;
    }

    /**
     * set shibboleth connection params
     *
     * @param array $shibboleth
     * @return self
     * @deprecated Utiliser setShib()
     */
    public function setShibboleth(array $shibboleth = []): self
    {
        return $this->setShib($shibboleth);
    }

    /**
     * get shibboleth connection params
     *
     * @return array
     * @deprecated Utiliser getShib()
     */
    public function getShibboleth(): array
    {
        return $this->getShib();
    }

    /**
     * set shibboleth connection params
     *
     * @return self
     */
    public function setShib(array $shib = []): self
    {
        $this->shib = $shib;

        return $this;
    }

    /**
     * get shibboleth connection params
     *
     * @return array
     */
    public function getShib(): array
    {
        return $this->shib;
    }

    /**
     * @return string
     */
    public function getEntityManagerName(): string
    {
        return $this->entityManagerName;
    }

    /**
     * @param string $entityManagerName
     * @return self
     */
    public function setEntityManagerName(string $entityManagerName): self
    {
        $this->entityManagerName = $entityManagerName;

        return $this;
    }

    /**
     * Cette classe hérite de {@see \Zend\Stdlib\AbstractOptions} qui impose d'avoir un getter correspondant à chaque
     * clé de config fournie. Cela empêche par ex unicaen/auth-token d'ajouter le type d'authentification 'token'
     * sans ajouter un getToken() à cette classe, ce qui serait une dépendance inversée.
     *
     * Donc l'idée est de pouvoir récupérer un bout de config même s'il n'y a pas de getter associé.
     *
     * @inheritDoc
     */
    public function __get($key)
    {
        try {
            return parent::__get($key);
        } catch (\BadMethodCallException $e) {
            if (array_key_exists($key, $this->originalOptions)) {
                return $this->originalOptions[$key];
            } else {
                throw $e;
            }
        }
    }
}