<?php

namespace UnicaenAuthentification\Options;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Interop\Container\ContainerInterface;
use UnicaenApp\Exception\RuntimeException;

/**
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ModuleOptionsFactory
{
    protected $class = ModuleOptions::class;

    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @return mixed
     */
    public function __invoke(ContainerInterface $container)
    {
        $config       = $container->get('Configuration');
        $moduleConfig = $config['unicaen-auth'] ?? [];
        $moduleConfig = array_merge($config['zfcuser'], $moduleConfig);

        $this->validateConfig($moduleConfig);

        return new $this->class($moduleConfig);
    }

    /**
     * @param array $config
     */
    protected function validateConfig(array $config)
    {
        //
        // Config authentification shibboleth.
        //
        if (array_key_exists('shibboleth', $config)) {
            throw new RuntimeException(
                "Vous devez renommer la clé de configuration 'unicaen-auth.shibboleth' en 'unicaen-auth.shib'."
            );
        }
        if (array_key_exists('shib', $config)) {
            $shibConfig = $config['shib'];
            try {
                Assertion::keyExists($shibConfig, $k = 'logout_url');
            } catch (AssertionFailedException $e) {
                throw new RuntimeException(
                    "La clé de configuration obligatoire suivante n'a pas été trouvée : 'unicaen-auth.shib.$k'."
                );
            }
        }

        //
        // Config authentification locale (ldap et db).
        //
        if (! array_key_exists($k = 'local', $config)) {
            throw new RuntimeException(
                "La clé de configuration obligatoire suivante n'a pas été trouvée : 'unicaen-auth.$k'."
            );
        }

        //
        // Config authentification Db.
        //
        if (array_key_exists($k = 'db', $config)) {
            throw new RuntimeException(
                "La clé de config 'unicaen-auth.$k' et son contenu doivent être déplacés sous la nouvelle clé 'unicaen-auth.local'"
            );
        }

        //
        // Config authentification LDAP.
        //
        if (array_key_exists($k = 'ldap', $config)) {
            throw new RuntimeException(
                "La clé de config 'unicaen-auth.$k' et son contenu doivent être déplacés sous la nouvelle clé 'unicaen-auth.local'"
            );
        }
    }
}