<?php

namespace UnicaenAuthentification\Form;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ZfcUser\Form;

class LoginFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options = $container->get('zfcuser_module_options');

        $form = new LoginForm(null, $options);
        $form->get('identity')->setLabel("Username");
        $form->setInputFilter(new Form\LoginFilter($options));

        return $form;
    }
}
