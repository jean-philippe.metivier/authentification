<?php

namespace UnicaenAuthentification\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuthentification\Authentication\Adapter\Ldap as LdapAdapter;
use UnicaenAuthentification\Options\ModuleOptions;
use Zend\Authentication\Storage\Session;
use Zend\Session\SessionManager;

class LdapFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $moduleOptions = null)
    {
        /** @var LdapPeopleMapper $mapper */
        $mapper = $container->get('ldap_people_mapper');

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $storage = new Ldap();
        $storage->setStorage(new Session(LdapAdapter::class, null, $sessionManager));
        $storage->setMapper($mapper);
        $storage->setModuleOptions($moduleOptions);

        return $storage;
    }
}