<?php

namespace UnicaenAuthentification\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\ShibService;
use Zend\Authentication\Storage\Session;
use Zend\Session\SessionManager;

class ShibFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $moduleOptions
     * @return Shib
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $moduleOptions = null)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $storage = new Shib();
        $storage->setStorage(new Session(\UnicaenAuthentification\Authentication\Adapter\Shib::class, null, $sessionManager));
        $storage->setShibService($shibService);
        $storage->setModuleOptions($moduleOptions);

        return $storage;
    }
}