<?php

namespace UnicaenAuthentification\Authentication;

class SessionIdentity
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $usurpateur;

    /**
     * SessionIdentity constructor.
     *
     * @param string $username
     * @param string $source
     */
    protected function __construct(string $username, string $source)
    {
        $this->type = $source;
        $this->username = $username;
    }

    /**
     * @param string $username
     * @param string $source
     * @return self
     */
    static public function newInstance(string $username, string $source): self
    {
        return new self($username, $source);
    }

    /**
     * @param string $usernameUsurpe
     * @param string $usernameUsurpateur
     * @param string $source
     * @return self
     */
    static public function newInstanceForUsurpation(string $usernameUsurpe, string $usernameUsurpateur, string $source): self
    {
        $inst = new self($usernameUsurpe, $source);
        $inst->setUsurpateur($usernameUsurpateur);

        return $inst;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }

    /**
     * @return bool
     */
    public function isUsurpation(): bool
    {
        return $this->usurpateur !== null;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function typeIs(string $type): bool
    {
        return $this->type === $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsurpateur(): ?string
    {
        return $this->usurpateur;
    }

    /**
     * @param string|null $usurpateur
     * @return self
     */
    public function setUsurpateur(string $usurpateur = null): self
    {
        $this->usurpateur = $usurpateur;
        return $this;
    }
}