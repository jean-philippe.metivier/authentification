<?php

namespace UnicaenAuthentification\Authentication;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Authentication\Storage\Chain;
use Zend\Authentication\AuthenticationService;

/**
 * Description of AuthenticationServiceFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AuthenticationServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthenticationService(
            $container->get(Chain::class),
            $container->get('ZfcUser\Authentication\Adapter\AdapterChain')
        );
    }
}