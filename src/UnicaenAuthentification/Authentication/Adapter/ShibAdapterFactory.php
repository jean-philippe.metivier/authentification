<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\ShibService;
use UnicaenAuthentification\Service\User;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Router\RouteInterface;

class ShibAdapterFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Shib
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null)
    {
        $adapter = new Shib();
        $adapter->setStorage(new Session(Shib::class));

        $this->injectDependencies($adapter, $container);

        return $adapter;
    }

    /**
     * @param Shib $adapter
     * @param ContainerInterface $container
     */
    private function injectDependencies(Shib $adapter, ContainerInterface $container)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);
        $adapter->setShibService($shibService);

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $container->get('zfcuser_auth_service');
        $adapter->setAuthenticationService($authenticationService);

        /** @var RouteInterface $router */
        $router = $container->get('router');
        $adapter->setRouter($router);

        /** @var User $userService */
        $userService = $container->get('unicaen-auth_user_service');
        $adapter->setUserService($userService);

        $options = array_merge(
            $container->get('zfcuser_module_options')->toArray(),
            $container->get('unicaen-auth_module_options')->toArray());
        $moduleOptions = new ModuleOptions($options);
        $adapter->setModuleOptions($moduleOptions);

        // type alias
        $substitut = $moduleOptions->getShib()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }
    }
}