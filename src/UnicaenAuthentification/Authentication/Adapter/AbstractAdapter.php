<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use UnicaenAuthentification\Authentication\SessionIdentity;
use Zend\Authentication\Storage\StorageInterface;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use ZfcUser\Authentication\Adapter\ChainableAdapter;

abstract class AbstractAdapter implements ChainableAdapter, ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Returns the persistent storage handler
     *
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * Sets the persistent storage handler
     *
     * @param  StorageInterface $storage
     * @return self Provides a fluent interface
     */
    public function setStorage(StorageInterface $storage): self
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * Check if this adapter is satisfied or not
     *
     * @return bool
     */
    public function isSatisfied(): bool
    {
        $storage = $this->getStorage()->read();
        return (isset($storage['is_satisfied']) && true === $storage['is_satisfied']);
    }

    /**
     * Set if this adapter is satisfied or not
     *
     * @param bool $bool
     * @return self
     */
    public function setSatisfied($bool = true): self
    {
        $storage = $this->getStorage()->read() ?: array();
        $storage['is_satisfied'] = $bool;
        $this->getStorage()->write($storage);
        return $this;
    }

    /**
     * @param string $username
     * @return SessionIdentity
     */
    protected function createSessionIdentity(string $username): SessionIdentity
    {
        return SessionIdentity::newInstance($username, $this->type);
    }

    /**
     * Called when user id logged out
     *
     * @param EventInterface $e
     */
    public function logout(EventInterface $e)
    {
        $this->getStorage()->clear();
    }

    /**
     * @inheritDoc
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $events->attach('authenticate', [$this, 'authenticate'], $priority);
        $events->attach('logout', [$this, 'logout'], $priority);
    }
}
