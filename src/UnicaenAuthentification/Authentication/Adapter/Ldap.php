<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenApp\Options\ModuleOptions;
use UnicaenAuthentification\Options\Traits\ModuleOptionsAwareTrait;
use UnicaenAuthentification\Service\User;
use Zend\Authentication\Adapter\Ldap as LdapAuthAdapter;
use Zend\Authentication\Exception\ExceptionInterface;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\EventManager\Event;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;

/**
 * LDAP authentication adpater
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Ldap extends AbstractAdapter implements EventManagerAwareInterface
{
    use ModuleOptionsAwareTrait;

    const TYPE = 'ldap';

    const USURPATION_USERNAMES_SEP = '=';

    const LDAP_AUTHENTIFICATION_FAIL = 'authentification.ldap.fail';

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var EventManager
     */
    protected $eventManager;

    /**
     * @var LdapAuthAdapter
     */
    protected $ldapAuthAdapter;

    /**
     * @var LdapPeopleMapper
     */
    protected $ldapPeopleMapper;

    /**
     * @var string
     */
    protected $usernameUsurpe;

    /**
     * @var User
     */
    private $userService;

    /**
     * @param User $userService
     */
    public function setUserService(User $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @var ModuleOptions
     */
    private $appModuleOptions;

    /**
     * @param ModuleOptions $appModuleOptions
     */
    public function setAppModuleOptions(ModuleOptions $appModuleOptions)
    {
        $this->appModuleOptions = $appModuleOptions;
    }

    /**
     * @inheritDoc
     */
//    public function authenticate(EventInterface $e): bool
    public function authenticate($e): bool
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        $event = $e->getTarget();
        /* @var $event AdapterChainEvent */

        if ($this->isSatisfied()) {
            try {
                $storage = $this->getStorage()->read();
            } catch (ExceptionInterface $event) {
                throw new RuntimeException("Erreur de lecture du storage");
            }
            $event
                ->setIdentity($storage['identity'])
                ->setCode(AuthenticationResult::SUCCESS)
                ->setMessages(['Authentication successful.']);
            return true;
        }

        $username = $event->getRequest()->getPost()->get('identity');
        $credential = $event->getRequest()->getPost()->get('credential');

        if (function_exists('mb_strtolower')) {
            $username = mb_strtolower($username, 'UTF-8');
        } else {
            $username = strtolower($username);
        }

        $success = $this->authenticateUsername($username, $credential);

        // Failure!
        if (!$success) {
            $event
                ->setCode(AuthenticationResult::FAILURE)
                ->setMessages([/*'LDAP bind failed.'*/]);
            $this->setSatisfied(false);
            return false;
        }

        // recherche de l'individu dans l'annuaire LDAP
        $ldapPeople = $this->getLdapPeopleMapper()->findOneByUsername($this->usernameUsurpe ?: $username);
        if (!$ldapPeople) {
            $event
                ->setCode(AuthenticationResult::FAILURE)
                ->setMessages([/*'Authentication failed.'*/]);
            $this->setSatisfied(false);
            return false;
        }

        $identity = $this->createSessionIdentity($this->usernameUsurpe ?: $username);

        $event->setIdentity($identity);
        $this->setSatisfied(true);
        try {
            $storage = $this->getStorage()->read();
            $storage['identity'] = $event->getIdentity();
            $this->getStorage()->write($storage);
        } catch (ExceptionInterface $event) {
            throw new RuntimeException("Erreur de concernant le storage");
        }
        $event
            ->setCode(AuthenticationResult::SUCCESS)
            ->setMessages(['Authentication successful.']);

        /* @var $userService User */
        $this->userService->userAuthenticated($ldapPeople);

        return true;
    }

    /**
     * Extrait le loginUsurpateur et le loginUsurpé si l'identifiant spécifé est de la forme
     * "loginUsurpateur=loginUsurpé".
     *
     * @param string $identifiant Identifiant, éventuellement de la forme "loginUsurpateur=loginUsurpé"
     * @return array
     * [loginUsurpateur, loginUsurpé] si l'identifiant est de la forme "loginUsurpateur=loginUsurpé" ;
     * [] sinon.
     */
    static public function extractUsernamesUsurpation(string $identifiant): array
    {
        if (strpos($identifiant, self::USURPATION_USERNAMES_SEP) > 0) {
            list($identifiant, $usernameUsurpe) = explode(self::USURPATION_USERNAMES_SEP, $identifiant, 2);

            return [
                $identifiant,
                $usernameUsurpe
            ];
        }

        return [];
    }

    /**
     * Authentifie l'identifiant et le mot de passe spécifiés.
     *
     * @param string $username Identifiant de connexion
     * @param string $credential Mot de passe
     * @return boolean
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface
     * @throws \Zend\Ldap\Exception\LdapException
     */
    public function authenticateUsername(string $username, string $credential): bool
    {
        // si 2 logins sont fournis, cela active l'usurpation d'identité (à n'utiliser que pour les tests) :
        // - le format attendu est "loginUsurpateur=loginUsurpé"
        // - le mot de passe attendu est celui du compte usurpateur (loginUsurpateur)
        $this->usernameUsurpe = null;
        $usernames = self::extractUsernamesUsurpation($username);
        if (count($usernames) === 2) {
            list ($username, $this->usernameUsurpe) = $usernames;
            if (!in_array($username, $this->moduleOptions->getUsurpationAllowedUsernames())) {
                $this->usernameUsurpe = null;
            }
        }

        // LDAP auth
        $result = $this->getLdapAuthAdapter()->setUsername($username)->setPassword($credential)->authenticate();

        // Envoi des erreurs LDAP dans un événement
        if (!$result->isValid()) {
            $messages = "LDAP ERROR : ";
            $errorMessages = $result->getMessages();
            if (count($errorMessages) > 0) {
                // On ne prend que les 2 premières lignes d'erreur (les suivantes contiennent souvent
                // les mots de passe de l'utilisateur, et les mot de passe dans les logs... bof bof).
                for ($i = 0; $i < 2 && count($errorMessages) >= $i; $i++) {
                    $messages .= $errorMessages[$i] . " ";
                }
            }
            $errorEvent = new Event(self::LDAP_AUTHENTIFICATION_FAIL, null, ['messages' => $messages]);
            $this->getEventManager()->triggerEvent($errorEvent);
        }

        $success = $result->isValid();

        // verif existence du login usurpé
        if ($this->usernameUsurpe) {
            // s'il nexiste pas, échec de l'authentification
            if (!@$this->getLdapAuthAdapter()->getLdap()->searchEntries(
                "(" . $this->moduleOptions->getLdapUsername() . "=$this->usernameUsurpe)"
            )) {
                $this->usernameUsurpe = null;
                $success = false;
            }
        }

        return $success;
    }

    /**
     * get ldap people mapper
     *
     * @return LdapPeopleMapper
     */
    public function getLdapPeopleMapper(): LdapPeopleMapper
    {
        return $this->ldapPeopleMapper;
    }

    /**
     * set ldap people mapper
     *
     * @param LdapPeopleMapper $mapper
     * @return self
     */
    public function setLdapPeopleMapper(LdapPeopleMapper $mapper): self
    {
        $this->ldapPeopleMapper = $mapper;
        return $this;
    }

    /**
     * @return ModuleOptions
     */
    public function getAppModuleOptions(): ModuleOptions
    {
        return $this->appModuleOptions;
    }

    /**
     * get ldap connection adapter
     *
     * @return LdapAuthAdapter
     */
    public function getLdapAuthAdapter(): LdapAuthAdapter
    {
        if (null === $this->ldapAuthAdapter) {
            $options = [];
            if (($config = $this->getAppModuleOptions()->getLdap())) {
                foreach ($config['connection'] as $name => $connection) {
                    $options[$name] = $connection['params'];
                }
            }
            $this->ldapAuthAdapter = new LdapAuthAdapter($options); // NB: array(array)
        }
        return $this->ldapAuthAdapter;
    }

    /**
     * set ldap connection adapter
     *
     * @param LdapAuthAdapter $authAdapter
     * @return self
     */
    public function setLdapAuthAdapter(LdapAuthAdapter $authAdapter): self
    {
        $this->ldapAuthAdapter = $authAdapter;
        return $this;
    }

    /**
     * Retrieve EventManager instance
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        return $this->eventManager;
    }

    /**
     * @inheritdoc
     */
    public function setEventManager(EventManagerInterface $eventManager): self
    {
        $eventManager->setIdentifiers(
            [
                __NAMESPACE__,
                __CLASS__,
            ]
        );
        $this->eventManager = $eventManager;

        return $this;
    }
}