<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use ZfcUser\Exception;

class AdapterChain extends \ZfcUser\Authentication\Adapter\AdapterChain
{
    /**
     * prepareForAuthentication
     *
     * @param  Request $request
     * @return Response|bool
     * @throws Exception\AuthenticationEventException
     */
    public function prepareForAuthentication(Request $request)
    {
        $e = $this->getEvent();
        $e->setRequest($request);

        $this->getEventManager()->trigger('authenticate.pre', $e);

        $result = $this->getEventManager()->triggerUntil(function ($result) {
            return $result === true || $result instanceof Response;
        }, 'authenticate', $e);

        if ($result->stopped()) {
            $last = $result->last();
            if ($last === true || $last instanceof Response) {
                return $last;
            }
//            throw new Exception\AuthenticationEventException(
//                sprintf(
//                    'Auth event was stopped without a response. Got "%s" instead',
//                    is_object($result->last()) ? get_class($result->last()) : gettype($result->last())
//                )
//            );
        }

        if ($e->getIdentity()) {
            $this->getEventManager()->trigger('authenticate.success', $e);
            return true;
        }

        $this->getEventManager()->trigger('authenticate.fail', $e);

        return false;
    }

    /**
     * logoutAdapters
     *
     * @return Response|null
     */
    public function logoutAdapters(): ?Response
    {
        //Adapters might need to perform additional cleanup after logout
        $responseCollection = $this->getEventManager()->triggerUntil(function ($test) {
            return ($test instanceof Response);
        }, 'logout', $this->getEvent());

        if ($responseCollection->stopped()) {
            if ($responseCollection->last() instanceof Response) {
                return $responseCollection->last();
            }

            throw new Exception\AuthenticationEventException(
                sprintf(
                    'Auth event was stopped without a response. Got "%s" instead',
                    is_object($responseCollection->last()) ? get_class($responseCollection->last()) : gettype($responseCollection->last())
                )
            );
        }

        return null;
    }
}
