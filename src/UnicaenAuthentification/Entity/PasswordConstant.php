<?php

namespace UnicaenAuthentification\Entity;

class PasswordConstant {
    const PASSWORD_LDAP = 'ldap';
    const PASSWORD_SHIB = 'shib';
}