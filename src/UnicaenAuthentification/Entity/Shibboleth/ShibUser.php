<?php

namespace UnicaenAuthentification\Entity\Shibboleth;

use UnicaenAuthentification\Entity\PasswordConstant;
use Webmozart\Assert\Assert;
use ZfcUser\Entity\UserInterface;

class ShibUser implements UserInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $displayName;

    /**
     * @var string
     */
    protected $nom;

    /**
     * @var string
     */
    protected $prenom;

    /**
     * @var string
     */
    protected $civilite;

    /**
     * @var int
     */
    protected $state = 1;

    /**
     * Teste si une chaîne ressemble à un EPPN.
     *
     * @param string $username
     * @return bool
     */
    static public function isEppn(string $username)
    {
        if (($pos = strpos($username, '@')) === false) {
            return false;
        }

        $domain = substr($username, $pos + 1);
        if (filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) === false) {
            return false;
        }

        return true;
    }

    /**
     * Extrait le domaine de l'EPPN spécifié.
     *
     * @param string $eppn
     * @return string
     */
    static public function extractDomainFromEppn(string $eppn)
    {
        Assert::true(static::isEppn($eppn), "La chaîne suivante n'est pas un EPPN valide : " . $eppn);

        $parts = explode('@', $eppn);

        return $parts[1];
    }

    /**
     * Retourne la partie domaine DNS de l'EPPN.
     * Retourne par exemple "unicaen.fr" lorsque l'EPPN est "tartempion@unicaen.fr"
     *
     * @return string
     */
    public function getEppnDomain()
    {
        return static::extractDomainFromEppn($this->getEppn());
    }

    /**
     * @return string
     */
    public function getEppn()
    {
        return $this->getUsername();
    }

    /**
     * Set eduPersoPrincipalName (EPPN).
     *
     * @param string $eppn eduPersoPrincipalName (EPPN), ex: 'gauthierb@unicaen.fr'
     */
    public function setEppn($eppn)
    {
        Assert::true(static::isEppn($eppn), "La chaîne suivante n'est pas un EPPN valide : " . $eppn);

        $this->setUsername($eppn);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param string $id supannEmpId ou supannEtuId
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string|null
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param string|null $civilite
     */
    public function setCivilite($civilite = null)
    {
        $this->civilite = $civilite;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return PasswordConstant::PASSWORD_SHIB;
    }

    /**
     * Set password.
     *
     * @param string $password
     */
    public function setPassword($password)
    {

    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getDisplayName();
    }
}