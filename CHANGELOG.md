# CHANGELOG

## 1.0.1 (07/02/2022)

- Ajout d'un paramètre "form_skip" qui permet de sauter la page d'authentification "/auth/connexion" si on utilise qu'une seule source d'authentification (pas utilisable pour une authentificatin locale).
- [Fix] La déconnexion du CAS n'était pas effective lors de la déconnexion de l'application

## 1.0.0 (02/02/2022)

- Scission du module UnicaenAuth en trois modules :
  - UnicaenAuthentification
  - UnicaenUtilisateur
  - UnicaenPrivilege

---

## UnicaenAuth (deprecated)

## 1.3.0 - 23/01/2019

- Authentification locale activable/désactivable dans la config.
    - Clé de config `unicaen-auth` > `local` > `enabled`.

- Ajout de la fonctionnalité "Mot de passe oublié" pour l'authentification locale. 
    - Principe: un lien contenant un token est envoyé par mail à l'utilisateur.
    - NB: Le username de l'utilisateur doit être une adresse électronique.
    - NB: nécessité de créer une nouvelle colonne dans la table des utilisateurs,
      cf. [répertoire data](./data).    

## 1.3.1 - 25/01/2019

- Fonctionnalité "Mot de passe oublié" :
    - Correction: l'utilisateur n'était pas recherché par son username!
    - Ajout d'un validateur sur le formulaire de saisie de l'adresse électronique.
    - Vérification que le compte utilisateur est bien local.

## 1.3.2 - 29/01/2019

- Authentification Shibboleth: possibilité de spécifier les attributs nécessaires au fonctionnement de l'appli 
  (clé de config `unicaen-auth` > `shibboleth` > `required_attributes`).
  
## 1.3.3 - 29/01/2019

- Correction du namespace de l'exception InvalidArgumentException lancée dans ShibService. 

## 1.3.4 - 30/01/2019

- Correction d'un nom erroné de variable passée à une vue : passwordReset.
- Correction config globale .dist: désactivation par défaut de l'auth locale pure ; commentaires plus précis.
- Adapter Db: suppression catch exception inexistante ; correction doc ; ajout initialisation de variable manquante.

## 1.3.5 - 04/02/2019

- Simplifications autour de la simulation d'une authentification shibboleth.
