<?php

use UnicaenAuthentification\Authentication\Adapter\AdapterChainServiceFactory;
use UnicaenAuthentification\Authentication\Adapter\Cas;
use UnicaenAuthentification\Authentication\Adapter\CasAdapterFactory;
use UnicaenAuthentification\Authentication\Adapter\Db;
use UnicaenAuthentification\Authentication\Adapter\DbAdapterFactory;
use UnicaenAuthentification\Authentication\Adapter\Ldap;
use UnicaenAuthentification\Authentication\Adapter\LdapAdapterFactory;
use UnicaenAuthentification\Authentication\Adapter\LocalAdapter;
use UnicaenAuthentification\Authentication\Adapter\LocalAdapterFactory;
use UnicaenAuthentification\Authentication\Adapter\Shib;
use UnicaenAuthentification\Authentication\Adapter\ShibAdapterFactory;
use UnicaenAuthentification\Authentication\AuthenticationServiceFactory;
use UnicaenAuthentification\Authentication\Storage\Auth;
use UnicaenAuthentification\Authentication\Storage\AuthFactory;
use UnicaenAuthentification\Authentication\Storage\DbFactory;
use UnicaenAuthentification\Authentication\Storage\LdapFactory;
use UnicaenAuthentification\Authentication\Storage\ShibFactory;
use UnicaenAuthentification\Authentication\Storage\Usurpation;
use UnicaenAuthentification\Authentication\Storage\UsurpationFactory;
use UnicaenAuthentification\Controller\AuthController;
use UnicaenAuthentification\Controller\AuthControllerFactory;
use UnicaenAuthentification\Controller\UtilisateurControllerFactory;
use UnicaenAuthentification\Event\EventManager;
use UnicaenAuthentification\Event\EventManagerFactory;
use UnicaenAuthentification\Form\CasLoginForm;
use UnicaenAuthentification\Form\CasLoginFormFactory;
use UnicaenAuthentification\Form\LoginForm;
use UnicaenAuthentification\Form\LoginFormFactory;
use UnicaenAuthentification\Form\ShibLoginForm;
use UnicaenAuthentification\Form\ShibLoginFormFactory;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\CasService;
use UnicaenAuthentification\Service\CasServiceFactory;
use UnicaenAuthentification\Service\ShibService;
use UnicaenAuthentification\Service\ShibServiceFactory;
use UnicaenAuthentification\Service\User;
use UnicaenAuthentification\Service\UserContext;
use UnicaenAuthentification\Service\UserContextFactory;
use UnicaenAuthentification\Service\UserFactory;
use UnicaenAuthentification\Service\UserMapperFactory;
use UnicaenAuthentification\View\Helper\CasConnectViewHelper;
use UnicaenAuthentification\View\Helper\CasConnectViewHelperFactory;
use UnicaenAuthentification\View\Helper\ConnectViewHelper;
use UnicaenAuthentification\View\Helper\DbConnectViewHelper;
use UnicaenAuthentification\View\Helper\DbConnectViewHelperFactory;
use UnicaenAuthentification\View\Helper\LdapConnectViewHelper;
use UnicaenAuthentification\View\Helper\LdapConnectViewHelperFactory;
use UnicaenAuthentification\View\Helper\LocalConnectViewHelper;
use UnicaenAuthentification\View\Helper\LocalConnectViewHelperFactory;
use UnicaenAuthentification\View\Helper\ShibConnectViewHelper;
use UnicaenAuthentification\View\Helper\ShibConnectViewHelperFactory;
use UnicaenAuthentification\View\Helper\UserConnection;
use UnicaenAuthentification\View\Helper\UserConnectionFactory;
use UnicaenAuthentification\View\Helper\UserUsurpationHelper;
use UnicaenAuthentification\View\Helper\UserUsurpationHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenPrivilege\Guard\PrivilegeControllerFactory;
use UnicaenPrivilege\Guard\PrivilegeRoute;
use UnicaenPrivilege\Guard\PrivilegeRouteFactory;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProviderFactory;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Proxy\LazyServiceFactory;

$settings = [
    /**
     * Tous les types d'authentification supportés par le module unicaen/auth.
     */
    'auth_types' => [
        'local', // càd 'ldap' et 'db'
        'cas',
        'shib',
    ],

    /**
     * Configuration de l'authentification centralisée (CAS).
     */
    'cas' => [
        /**
         * Ordre d'affichage du formulaire de connexion.
         */
        'order' => 1,

        /**
         * Activation ou non de ce mode d'authentification.
         */
        'enabled' => true,

        /**
         * Description facultative de ce mode d'authentification qui apparaîtra sur la page de connexion.
         */
        'description' => "Cliquez sur le bouton ci-dessous pour accéder à l'authentification centralisée.",

        /**
         * Adapter compétent pour réaliser l'authentification de l'utilisateur.
         */
        'adapter' => Cas::class,

        /**
         * Service/formulaire d'authentification à utiliser.
         */
        'form' => CasLoginForm::class,

        /**
         * Permet de sauter le formulaire d'authentification "/auth/connexion" si CAS est la seule source d'authentification
         */
        'form_skip' => false,

        /**
         * Infos de connexion au serveur CAS.
         */
        'connection' => [
            'default' => [
                'params' => [
                    'hostname' => 'host.domain.fr',
                    'port' => 443,
                    'version' => "2.0",
                    'uri' => "",
                    'debug' => false,
                ],
            ],
        ]
    ],

    /**
     * Configuration de l'authentification locale (compte LDAP établissement, ou compte BDD application).
     */
    'local' => [
        'order' => 2,
        'enabled' => true,
        'description' => "Utilisez ce formulaire si vous possédez un compte LDAP établissement ou un compte local dédié à l'application.",

        'form' => LoginForm::class,

        /** /!\ Pas modifiable dans la conf local */
        'form_skip' => false,

        /**
         * Mode d'authentification à l'aide d'un compte dans la BDD de l'application.
         */
        'db' => [
            'enabled' => true, // doit être activé pour que l'usurpation fonctionne (cf. Authentication/Storage/Db::read()) :-/ todo: faire mieux
            'adapter' => Db::class,
            'form' => LoginForm::class,
        ],

        /**
         * Mode d'authentification à l'aide d'un compte LDAP.
         */
        'ldap' => [
            'enabled' => false,
            'adapter' => Ldap::class,
            'form' => LoginForm::class,
        ],
    ],

    /**
     * Configuration de l'authentification via la fédération d'identité (Shibboleth).
     */
    'shib' => [
        'order' => 3,
        'enabled' => false,
        'description' => "Cliquez sur le bouton ci-dessous pour accéder à l'authentification via la fédération d'identité.",
        'adapter' => Shib::class,
        'form' => ShibLoginForm::class,

        /**
         * Permet de sauter le formulaire d'authentification "/auth/connexion" si Shibboleth est la seule source d'authentification
         */
        'form_skip' => false,

        /**
         * URL de déconnexion.
         */
        //'logout_url' => '/Shibboleth.sso/Logout?return=', // NB: '?return=' semble obligatoire!

        /*
        'simulate' => [
            'eppn'        => 'login@domain.fr',
            'supannEmpId' => '00012345',
        ],
        'aliases' => [
            'eppn'                   => 'HTTP_EPPN',
            'mail'                   => 'HTTP_MAIL',
            'eduPersonPrincipalName' => 'HTTP_EPPN',
            'supannEtuId'            => 'HTTP_SUPANNETUID',
            'supannEmpId'            => 'HTTP_SUPANNEMPID',
            'supannCivilite'         => 'HTTP_SUPANNCIVILITE',
            'displayName'            => 'HTTP_DISPLAYNAME',
            'sn'                     => 'HTTP_SN',
            'givenName'              => 'HTTP_GIVENNAME',
        ],
        /*
        'required_attributes' => [
            'eppn',
            'mail',
            'eduPersonPrincipalName',
            'supannCivilite',
            'displayName',
            'sn|surname', // i.e. 'sn' ou 'surname'
            'givenName',
            'supannEtuId|supannEmpId',
        ],
        */

        /**
         * Configuration de la stratégie d'extraction d'un identifiant utile parmi les données d'authentification
         * shibboleth.
         * Ex: identifiant de l'usager au sein du référentiel établissement, transmis par l'IDP via le supannRefId.
         */
        'shib_user_id_extractor' => [
            /*
            // domaine (ex: 'unicaen.fr') de l'EPPN (ex: hochonp@unicaen.fr')
            'unicaen.fr' => [
                'supannRefId' => [
                    // nom du 1er attribut recherché
                    'name' => 'supannRefId', // ex: '{OCTOPUS:ID}1234;{ISO15693}044D1AZE7A5P80'
                    // pattern éventuel pour extraire la partie intéressante
                    'preg_match_pattern' => '|\{OCTOPUS:ID\}(\d+)|', // ex: permet d'extraire '1234'
                ],
                'supannEmpId' => [
                    // nom du 2e attribut recherché, si le 1er est introuvable
                    'name' => 'supannEmpId',
                    // pas de pattern donc valeur brute utilisée
                    'preg_match_pattern' => null,
                ],
                'supannEtuId' => [
                    // nom du 3e attribut recherché, si le 2e est introuvable
                    'name' => 'supannEtuId',
                ],
            ],
            */
            // config de repli pour tous les autres domaines
            'default' => [
                'supannEmpId' => [
                    'name' => 'supannEmpId',
                ],
                'supannEtuId' => [
                    'name' => 'supannEtuId',
                ],
            ],
        ],
    ],

    /**
     * Fournisseurs d'identité.
     */
    'identity_providers' => [
        300 => 'UnicaenAuthentification\Provider\Identity\Basic', // en 1er
        200 => 'UnicaenAuthentification\Provider\Identity\Db',    // en 2e
        100 => 'UnicaenAuthentification\Provider\Identity\Ldap',  // en 3e @deprecated
    ],

    /**
     * Attribut LDAP utilisé pour le username des utilisateurs
     */
    'ldap_username' => 'supannaliaslogin',
];

return [
    'zfcuser' => [
        /**
         * Enable registration
         * Allows users to register through the website.
         * Accepted values: boolean true or false
         */
        'enable_registration' => true,
        /**
         * Modes for authentication identity match
         * Specify the allowable identity modes, in the order they should be
         * checked by the Authentication plugin.
         * Default value: array containing 'email'
         * Accepted values: array containing one or more of: email, username
         */
        'auth_identity_fields' => ['username', 'email'],
        /**
         * Login Redirect Route
         * Upon successful login the user will be redirected to the entered route
         * Default value: 'zfcuser'
         * Accepted values: A valid route name within your application
         */
        'login_redirect_route' => 'home',
        /**
         * Logout Redirect Route
         * Upon logging out the user will be redirected to the enterd route
         * Default value: 'zfcuser/login'
         * Accepted values: A valid route name within your application
         */
        'logout_redirect_route' => 'home',
        /**
         * Enable Username
         * Enables username field on the registration form, and allows users to log
         * in using their username OR email address. Default is false.
         * Accepted values: boolean true or false
         */
        'enable_username' => false,
        /**
         * Enable Display Name
         * Enables a display name field on the registration form, which is persisted
         * in the database. Default value is false.
         * Accepted values: boolean true or false
         */
        'enable_display_name' => true,
        /**
         * Authentication Adapters
         * Specify the adapters that will be used to try and authenticate the user
         * Default value: array containing 'ZfcUser\Authentication\Adapter\Db' with priority 100
         * Accepted values: array containing services that implement 'ZfcUser\Authentication\Adapter\ChainableAdapter'
         */
        'auth_adapters' => [
            400 => LocalAdapter::class, // délègue à Db et Ldap
            100 => 'UnicaenAuthentification\Authentication\Adapter\Cas',
            50 => 'UnicaenAuthentification\Authentication\Adapter\Shib',
        ],

        // telling ZfcUser to use our own class
        'user_entity_class' => 'UnicaenAuthentification\Entity\Db\User',
        // telling ZfcUserDoctrineORM to skip the entities it defines
        'enable_default_entities' => false,
    ],
    'bjyauthorize' => [

        // strategy service name for the strategy listener to be used when permission-related errors are detected
        //    'unauthorized_strategy' => 'BjyAuthorize\View\RedirectionStrategy',
        'unauthorized_strategy' => 'UnicaenAuthentification\View\RedirectionStrategy',

        /* Currently, only controller and route guards exist
         */
        'guards' => [
            /* If this guard is specified here (i.e. it is enabled), it will block
             * access to all controllers and actions unless they are specified here.
             * You may omit the 'action' index to allow access to the entire controller
             */
            'BjyAuthorize\Guard\Controller' => [
                ['controller' => 'index', 'action' => 'index', 'roles' => 'guest'],
                ['controller' => 'zfcuser', 'roles' => 'guest'],
                ['controller' => 'Application\Controller\Index', 'roles' => 'guest'],

                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'etab', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'apropos', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'contact', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'plan', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'mentions-legales', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'informatique-et-libertes', 'roles' => 'guest'],
                ['controller' => 'UnicaenApp\Controller\Application', 'action' => 'refresh-session', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Utilisateur', 'action' => 'selectionner-profil', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Utilisateur', 'action' => 'usurper-identite', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Utilisateur', 'action' => 'stopper-usurpation', 'roles' => 'guest'],

                ['controller' => 'UnicaenAuthentification\Controller\Auth', 'action' => 'login', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Auth', 'action' => 'authenticate', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Auth', 'action' => 'logout', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Auth', 'action' => 'shibboleth', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Auth', 'action' => 'requestPasswordReset', 'roles' => 'guest'],
                ['controller' => 'UnicaenAuthentification\Controller\Auth', 'action' => 'changePassword', 'roles' => 'guest'],
            ],
        ],
    ],
    'unicaen-auth' => $settings,
    'doctrine' => [
        'driver' => [
            // overriding zfc-user-doctrine-orm's config
            'zfcuser_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [
                    __DIR__ . '/../src/UnicaenAuthentification/Entity/Db',
                ],
            ],
            'orm_auth_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/UnicaenAuthentification/Entity/Db',
                ],
            ],
            'orm_default' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => [
//                    'UnicaenAuthentification\Entity\Db' => 'zfcuser_entity',
                    'UnicaenAuthentification\Entity\Db' => 'orm_auth_driver',
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'error/403' => __DIR__ . '/../view/error/403.phtml',
        ],
        'template_path_stack' => [
            'unicaen-autorisation' => __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'auth' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/auth',
                    'defaults' => [
                        'controller' => 'UnicaenAuthentification\Controller\Auth',
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'shibboleth' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/shibboleth',
                            'defaults' => [
                                'action' => 'shibboleth',
                            ],
                        ],
                    ],
                    'requestPasswordReset' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/request-password-reset',
                            'defaults' => [
                                'action' => 'requestPasswordReset',
                            ],
                        ],
                    ],
                    'changePassword' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/change-password/:token',
                            'defaults' => [
                                'action' => 'changePassword',
                            ],
                        ],
                    ],
                ],
            ],
            'zfcuser' => [
                'type' => 'Literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/auth',
                    'defaults' => [
                        'controller' => 'zfcuser',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'login' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/connexion[/:type]',
                            'defaults' => [
                                'controller' => 'UnicaenAuthentification\Controller\Auth', // remplace 'zfcuser'
                                'action' => 'login',
                            ],
                        ],
                    ],
                    'authenticate' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/authenticate/:type',
                            'defaults' => array(
                                'controller' => 'UnicaenAuthentification\Controller\Auth', // remplace 'zfcuser'
                                'action' => 'authenticate',
                            ),
                        ),
                    ),
                    'logout' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/deconnexion',
                            'defaults' => [
                                'controller' => 'UnicaenAuthentification\Controller\Auth', // remplace 'zfcuser'
                                'action' => 'logout',
                            ],
                        ],
                    ],
                    'register' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/creation-compte',
                            'defaults' => [
                                'controller' => 'zfcuser',
                                'action' => 'register',
                            ],
                        ],
                    ],
                ],
            ],
            'utilisateur' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/utilisateur',
                    'defaults' => [
                        '__NAMESPACE__' => 'UnicaenAuthentification\Controller',
                        'controller' => 'Utilisateur',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/:action[/:id]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]*',
                            ],
                            'defaults' => [
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    // All navigation-related configuration is collected in the 'navigation' key
    'navigation' => [
        // The DefaultNavigationFactory we configured uses 'default' as the sitemap key
        'default' => [
            // And finally, here is where we define our page hierarchy
            'home' => [
                'pages' => [
                    'login' => [
                        'label' => _("Connexion"),
                        'route' => 'zfcuser/login',
                        'visible' => false,
                    ],
                    'register' => [
                        'label' => _("Enregistrement"),
                        'route' => 'zfcuser/register',
                        'visible' => false,
                    ],
                ],
            ],
        ],
    ],
    //
    //( ! ) Warning: Declaration of
    // Application\Service\UserContextServiceAwareTrait::setUserContextService(Application\Service\UserContextService $userContextService)
    // should be compatible with
    // UnicaenAuthentification\Controller\UtilisateurController::setUserContextService(UnicaenAuthentification\Service\UserContext $userContextService)
    // in /var/www/sygal/module/Application/src/Application/Controller/UtilisateurController.php on line 34
    'service_manager' => [
        'aliases' => [
            'unicaen-auth_module_options' => ModuleOptions::class,
            'zfcuser_login_form' => LoginForm::class,
            AuthenticationService::class => 'zfcuser_auth_service',
            'Zend\Authentication\AuthenticationService' => 'zfcuser_auth_service',
            'unicaen-auth_user_service' => User::class, // pour la compatibilité
            'authUserContext' => UserContext::class, // pour la compatibilité
            'AuthUserContext' => UserContext::class, // pour la compatibilité
        ],
        'invokables' => [
            'UnicaenAuthentification\View\RedirectionStrategy' => 'UnicaenAuthentification\View\RedirectionStrategy',
        ],
        'factories' => [
            ModuleOptions::class => 'UnicaenAuthentification\Options\ModuleOptionsFactory',
            'zfcuser_auth_service' => AuthenticationServiceFactory::class,
            AuthenticationService::class => AuthenticationServiceFactory::class,
            'UnicaenAuthentification\Authentication\Storage\Chain' => 'UnicaenAuthentification\Authentication\Storage\ChainServiceFactory',
            'UnicaenAuthentification\Provider\Identity\Chain' => 'UnicaenAuthentification\Provider\Identity\ChainServiceFactory',
            'UnicaenAuthentification\Provider\Identity\Ldap' => 'UnicaenAuthentification\Provider\Identity\LdapServiceFactory',
            'UnicaenAuthentification\Provider\Identity\Db' => 'UnicaenAuthentification\Provider\Identity\DbServiceFactory',
            'UnicaenAuthentification\Provider\Identity\Basic' => 'UnicaenAuthentification\Provider\Identity\BasicServiceFactory',
            'zfcuser_redirect_callback' => 'UnicaenAuthentification\Authentication\RedirectCallbackFactory', // substituion
            CasService::class => CasServiceFactory::class,
            ShibService::class => ShibServiceFactory::class,
            UserContext::class => UserContextFactory::class,
            'zfcuser_user_mapper' => UserMapperFactory::class,

            LoginForm::class => LoginFormFactory::class,
            CasLoginForm::class => CasLoginFormFactory::class,
            ShibLoginForm::class => ShibLoginFormFactory::class,

            // ADAPTATEUR ----------------------------------------------------------------------------------------------
            'ZfcUser\Authentication\Adapter\AdapterChain' => AdapterChainServiceFactory::class,
            LocalAdapter::class => LocalAdapterFactory::class,
            'UnicaenAuthentification\Authentication\Adapter\Cas' => CasAdapterFactory::class,
            'UnicaenAuthentification\Authentication\Adapter\Db' => DbAdapterFactory::class,
            'UnicaenAuthentification\Authentication\Adapter\Ldap' => LdapAdapterFactory::class,
            'UnicaenAuthentification\Authentication\Adapter\Shib' => ShibAdapterFactory::class,

            // STORAGE -------------------------------------------------------------------------------------------------
            Auth::class => AuthFactory::class,
            'UnicaenAuthentification\Authentication\Storage\Db' => DbFactory::class,
            'UnicaenAuthentification\Authentication\Storage\Ldap' => LdapFactory::class,
            'UnicaenAuthentification\Authentication\Storage\Shib' => ShibFactory::class,
            Usurpation::class => UsurpationFactory::class,

            'UnicaenAuthentification\Service\User' => UserFactory::class,
            EventManager::class => EventManagerFactory::class,


            /** @TODO Bertrand comprendre pourquoi on ne peut pas bouger çà ... @TODO * */
            PrivilegeController::class => PrivilegeControllerFactory::class,
            PrivilegeRoute::class => PrivilegeRouteFactory::class,
            'UnicaenAuthentification\Provider\Rule\PrivilegeRuleProvider' => PrivilegeRuleProviderFactory::class,

        ],
        'lazy_services' => [
            // Mapping services to their class names is required since the ServiceManager is not a declarative DIC.
            'class_map' => [
                'zfcuser_auth_service' => AuthenticationService::class,
            ],
        ],
        'delegators' => [
            'zfcuser_auth_service' => [
                LazyServiceFactory::class,
            ],
        ],
        'initializers' => [
            'UnicaenAuthentification\Service\UserAwareInitializer',
        ],
    ],

    'controllers' => [
        'aliases' => [
            'UnicaenAuthentification\Controller\Auth' => AuthController::class,
        ],
        'factories' => [
            AuthController::class => AuthControllerFactory::class,
            'UnicaenAuthentification\Controller\Utilisateur' => UtilisateurControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [

        ]
    ],

    'view_helpers' => [
        'aliases' => [
            'userConnection' => UserConnection::class,
            'userUsurpation' => UserUsurpationHelper::class,
            'dbConnect' => DbConnectViewHelper::class,
            'localConnect' => LocalConnectViewHelper::class,
            'ldapConnect' => LdapConnectViewHelper::class,
            'shibConnect' => ShibConnectViewHelper::class,
            'connect' => ConnectViewHelper::class,
            'casConnect' => CasConnectViewHelper::class,
        ],
        'factories' => [
            UserConnection::class => UserConnectionFactory::class,
            UserUsurpationHelper::class => UserUsurpationHelperFactory::class,
            CasConnectViewHelper::class => CasConnectViewHelperFactory::class,
            DbConnectViewHelper::class => DbConnectViewHelperFactory::class,
            LocalConnectViewHelper::class => LocalConnectViewHelperFactory::class,
            LdapConnectViewHelper::class => LdapConnectViewHelperFactory::class,
            ShibConnectViewHelper::class => ShibConnectViewHelperFactory::class,
        ],
        'invokables' => [
            'appConnection' => 'UnicaenAuthentification\View\Helper\AppConnection',
            ConnectViewHelper::class,
        ],
    ],
];